//
// Created by MERHAB on 29/08/2023.
//
#pragma once
#include <QLineEdit>
#include "MDbAbleGui.h"
#include <QKeyEvent>
#include <QFocusEvent>
#include "MMessageBox.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "QLabel"
/*
 *
 *   explicit MLineEdit(MField *fld , QWidget *parent = nullptr)
            : QWidget(parent), MDbAbleGui(fld) {
        caption.setText(fld->getMeta()->getCaption());
        this->setLayout(&vLayout);
        hLayout.addWidget(&caption);
        hLayout.addWidget(&edit);
        vLayout.addLayout(&hLayout);
        vLayout.addWidget(&infoLabel);
        class MLineEdit: public QWidget,public MDbAbleGui{
    QLineEdit edit;
    QLabel caption;
    QLabel infoLabel;
    QHBoxLayout hLayout;
    QVBoxLayout vLayout;
public:
    }
 */
class MLineEdit: public QLineEdit,public MDbAbleGui{
public:
    enum{MLineEditValidate};
explicit MLineEdit(MField* fld ,QWidget* parent = nullptr):
QLineEdit(parent), MDbAbleGui(fld){

}

void setValue(QVariant value) override;
QVariant value() override;
   void keyPressEvent(QKeyEvent *event) override;
   void focusOutEvent(QFocusEvent *event) override;

    MValidationResult isValid() override;
};


