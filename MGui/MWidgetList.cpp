//
// Created by mac on 29/08/2023.
//

#include "MWidgetList.h"

MWidgetList *MWidgetList::appendLine(QWidgetList list) {
    this->widgetsList.append(list);
    return this;
}

MWidgetList *MWidgetList::addWidget(QWidget *widget, int line) {
    assert(line>=0 and line< this->widgetsList.size());
    this->widgetsList[line].append(widget);
    this->gLayout.addWidget(widget,line, this->widgetsList[line].size());
    return this;
}

 QGridLayout *MWidgetList::getGLayout()  {
    return &gLayout;
}

MWidgetList::MWidgetList(QVector<QWidgetList> widgetsList) {
    this->widgetsList =widgetsList;
    fillGLayout();
}

MWidgetList::MWidgetList(const MWidgetList &list) {

}

void MWidgetList::fillGLayout() {
    for (int i = 0; i < this->gLayout.columnCount(); ++i) {
        for (int j = 0; j < this->gLayout.rowCount() ; ++j) {
            gLayout.removeWidget((QWidget*)gLayout.itemAtPosition(j,i));
        }
    }
    int maxCol=0;
    for (int i = 0; i < widgetsList.size() ; ++i) {
        if (maxCol < widgetsList[i].size())
            maxCol=widgetsList[i].size();
    }
    for (int i = 0; i < widgetsList.size() ; ++i) {
        for (int j = 0; j <widgetsList[i].size() ; ++j) {
            if(j == widgetsList[i].size()-1 and widgetsList[i].size()<maxCol){
                gLayout.addWidget(widgetsList[i][j],i,j,1,maxCol-widgetsList[i].size()+1);
            }else
            gLayout.addWidget(widgetsList[i][j],i,j);
        }
    }
}

