//
// Created by MERHAB on 29/08/2023.
//

#pragma once
#include <QWidget>
#include <QGridLayout>

class MWidgetList {
    QVector<QWidgetList> widgetsList;
    QGridLayout gLayout;
    void fillGLayout();
public:
    explicit MWidgetList(QVector<QWidgetList> widgetsList);

    MWidgetList(const MWidgetList& list);

    QGridLayout *getGLayout() ;
    MWidgetList* appendLine(QWidgetList list ={});
    MWidgetList* addWidget(QWidget* widget,int line);


};


