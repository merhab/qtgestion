//
// Created by MERHAB on 29/08/2023.
//
#pragma once
#include <QHBoxLayout>

class MHBoxLayout: public QHBoxLayout{
public:
    explicit MHBoxLayout(QWidget* parent = nullptr):QHBoxLayout(parent){}
    MHBoxLayout* addWidget(QWidget* widget);
    MHBoxLayout* addLayout(QLayout* layout);

};
MHBoxLayout* newMHBoxLayout(QWidget* parent = nullptr);

