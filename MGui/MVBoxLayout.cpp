//
// Created by mac on 29/08/2023.
//

#include "MVBoxLayout.h"

MVBoxLayout *MVBoxLayout::addWidget(QWidget *widget) {
     QVBoxLayout::addWidget(widget);
    return this;
}

MVBoxLayout *MVBoxLayout::addLayout(QLayout *layout) {
    QVBoxLayout::addLayout(layout);
    return this;
}

MVBoxLayout *newMVBoxLayout(QWidget *parent) {
    return new MVBoxLayout(parent);
}
