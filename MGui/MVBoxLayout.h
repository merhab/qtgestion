//
// Created by MERHAB on 29/08/2023.
//
#pragma once
#include <QVBoxLayout>

class MVBoxLayout: public QVBoxLayout{
public:
    explicit MVBoxLayout(QWidget* parent= nullptr): QVBoxLayout(parent){}
    MVBoxLayout* addWidget(QWidget* widget);
    MVBoxLayout* addLayout(QLayout* layout);
};
MVBoxLayout* newMVBoxLayout(QWidget* parent= nullptr) ;


