//
// Created by mac on 29/08/2023.
//

#include "MHBoxLayout.h"

MHBoxLayout *MHBoxLayout::addWidget(QWidget *widget) {
     QHBoxLayout::addWidget(widget);
    return this;
}

MHBoxLayout *MHBoxLayout::addLayout(QLayout *layout) {
    QHBoxLayout::addLayout(layout);
    return this;
}

MHBoxLayout *newMHBoxLayout(QWidget *parent) {
    return new MHBoxLayout(parent);
}
