//
// Created by MERHAB on 26/08/2023.
//
#include <QPushButton>
#include "MDbAbleGui.h"
#include <QMouseEvent>
class MPushButton: public QPushButton , public MDbAbleGui {
public:
    enum {OnClicked};
    explicit MPushButton(QWidget* parent = nullptr):QPushButton(parent),MDbAbleGui(nullptr){}
    void mouseReleaseEvent(QMouseEvent *) override;
     void setValue(QVariant variant) override;
    void appendOnClickedEvent(MEventFunc onClicked,void* receiver);
     QVariant value() override;
    MValidationResult isValid() override;
};


