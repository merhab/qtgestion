#include <QApplication>
#include "MPushButton.h"
#include <QFrame>
#include "MLabeledEdit.h"
#include "MHBoxLayout.h"
#include "MVBoxLayout.h"
#include "MWidgetList.h"

QVariantList onButtonClicked(void* sender, void* receiver, QVariantList args){
    static bool clicked = true;
    if(clicked){
        ((MPushButton*) sender)->setText("clicked");
        clicked = !clicked;
    } else{
        ((MPushButton*) sender)->setText(" not clicked");
        clicked = !clicked;
    }
    return {};
}
int main(int argc,char **argv){
  QApplication app(argc,argv);
  char ss[] = "مرحاب 🤣";
  QString s(ss);
  Meta metaName = metaStr(s);
  Meta metaAge = metaInt("age");
  MField fldName(&metaName,"nour");
  MField fldAge(&metaAge,45);
  QFrame frame;
    MVBoxLayout vlayout;
  MHBoxLayout layout;
  vlayout.addLayout(&layout);
  frame.setLayout(&vlayout);
  MPushButton button;
  button.appendOnClickedEvent(onButtonClicked,&button);
  layout.addWidget(&button);
  layout.addWidget(newMLabelEdit(&fldName,&frame));
  layout.addWidget(newMLabelEdit(&fldAge,&frame));
  vlayout.addLayout(newMHBoxLayout()->addWidget(
          newMLabelEdit(
                  newMField(
                          &newMetaStr("tel")->
                          setCaption("mobile in Algeria"),"mascara")))->addWidget(newMLabelEdit(
          newMField(
                  &newMetaStr("address")->
                          setCaption("baba ali"),"mascara"))));
  MWidgetList list({
      {
          new QLabel("name"),
          new QLineEdit("Noureddeien"),
          new QLabel("age"),
          new QLineEdit("45"),
          new QLabel("mmmm"),
          new QLineEdit("87768")
      },
      {
              new QLabel("jhldksj"),
              new QLineEdit("Nourekjsndblkjddeien"),
              new QLabel("lkj d"),
              new QLineEdit("65"),

      },
      {
              new QLabel("kjbnsd"),
              new QLineEdit("kanns"),
              new QLabel("skdjhnfjks"),
              new QLineEdit("44356"),
              new QLabel("lkjdh"),
              new QLineEdit("12")
      }
  });
  vlayout.addLayout(list.getGLayout());
  frame.show();
  app.exec();
  return 0;
}





