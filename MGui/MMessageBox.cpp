//
// Created by mac on 29/08/2023.
//

#include "MMessageBox.h"

MMessageBox::MMessageBox(const QString &message, QWidget *parent)
: QMessageBox(QMessageBox::Information,"information",message,QMessageBox::Ok,parent)
{

}
