//
// Created by mac on 26/08/2023.
//

#include "MPushButton.h"

void MPushButton::mouseReleaseEvent(QMouseEvent *event) {
    this->runEvents(OnClicked);
}

void MPushButton::setValue(QVariant variant) {

}

QVariant MPushButton::value() {
    return QVariant();
}

void MPushButton::appendOnClickedEvent(MEventFunc onClicked,void* recever) {
    this->appendEventList(MEventList(this,recever).
    registerEvent(MEvent(onClicked,OnClicked)));
}

MValidationResult MPushButton::isValid() {
    return {true,""};
}
