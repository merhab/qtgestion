//
// Created by mac on 29/08/2023.
//

#include "MLabeledEdit.h"

void MLabeledEdit::setValue(QVariant value) {
    edit.setValue(value);
}

QVariant MLabeledEdit::value() {
    return edit.value();
}

void MLabeledEdit::keyPressEvent(QKeyEvent *event) {
    edit.keyPressEvent(event);
}

void MLabeledEdit::focusOutEvent(QFocusEvent *event) {
    edit.focusOutEvent(event);
    if(event->isAccepted())
    QWidget::focusOutEvent(event);
}

MValidationResult MLabeledEdit::isValid() {
    return edit.isValid();
}

MLabeledEdit *newMLabelEdit(MField *fld, QWidget *parent) {
    return new MLabeledEdit(fld,parent);
}
