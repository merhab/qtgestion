//
// Created by MERHAB on 29/08/2023.
//
#pragma once
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include "MLineEdit.h"
#include "mtest.h"

class MLabeledEdit :public QWidget,public MDbAbleGui {
    MLineEdit edit;
    QLabel label;
    QLabel infoLabel;
    QVBoxLayout vLayout;
    QHBoxLayout hLayout;
public:
    explicit MLabeledEdit(MField *fld, QWidget *parent = nullptr)
            : QWidget(parent), MDbAbleGui(fld), edit(fld, parent) {
        label.setText(fld->getMeta()->getCaption());
        this->setLayout(&vLayout);
        vLayout.addLayout(&hLayout);
        hLayout.addWidget(&label);
        hLayout.addWidget(&edit);
        vLayout.addWidget(&infoLabel);
        if(fld) MLabeledEdit::setValue(fld->getValue());
    }

    void setValue(QVariant value) override;

    QVariant value() override;

    void keyPressEvent(QKeyEvent *event) override;

    void focusOutEvent(QFocusEvent *event) override;

    MValidationResult isValid() override;
};
MLabeledEdit* newMLabelEdit(MField *fld, QWidget *parent = nullptr);