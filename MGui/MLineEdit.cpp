//
// Created by mac on 29/08/2023.
//

#include "MLineEdit.h"

void MLineEdit::setValue(QVariant value) {
    this->setText(value.toString());
}

QVariant MLineEdit::value() {
    MSqlType type = this->field()->getMeta()->getType();
    bool ok;
    switch (type) {

        case Int: {
            int i = this->text().toInt(&ok);
            if (ok) {
                return i;
            } else
                assert(ok);
        }
            break;
        case Int64: {
            int64_t i = this->text().toLongLong(&ok);
            if (ok) {
                return i;
            } else
                assert(ok);
        }

            break;
        case String: {
            return this->text();

        }
            break;
        case Float: {
            float i = this->text().toFloat(&ok);
            if (ok) {
                return i;
            } else
                assert(ok);
        }
            break;
        case Null:
            return QVariant();
            break;
        default:
            assert(false);
            break;
    }
    return {};
}

void MLineEdit::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Return or event->key() == Qt::Key_Enter) {
        this->focusNextChild();
        // user validate the input
//        assert(field());
//        if (field()->getValue() != value() and isValid()) {
//                this->runEvents(MLineEditValidate);
//            //this->focusNextChild();
//                //QLineEdit::keyPressEvent(event);
//        }
    } else{
        QLineEdit::keyPressEvent(event);
    }
}

    MValidationResult MLineEdit::isValid() {
        return {false,""};
    }

    void MLineEdit::focusOutEvent(QFocusEvent *event) {
        assert(field());
        if (field()->getValue() != value()) {
            auto res = isValid();
            if (res.isValid) {
                this->runEvents(MLineEditValidate);
            } else {
                this->setFocus();
                //QMessageBox(QMessageBox::Information,"information",res.message,QMessageBox::Ok).exec();
                event->ignore();

                return;
            }
        }
        QLineEdit::focusOutEvent(event);
    }

