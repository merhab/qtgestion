//
// Created by MERHAB on 01/08/2023.
//
#pragma once
#include <qvariant.h>

struct NamedVal{
  QString name;
  QVariant val;
};
enum MStmtKind{AndFilter,OrFilter,Select,Update,OrderBy,CreateTable,Insert,Delete};
inline QString $(MStmtKind kind){
    switch (kind) {

        case AndFilter:
            return "AndFilter";
            break;
        case OrFilter:
            return "OrFilter";
            break;
        case Select:
            return "Select";
            break;
        case Update:
            return "Update";
            break;
        case OrderBy:
            return "OrderBy";
            break;
        case CreateTable:
            return "CreateTable";
            break;
        case Insert:
            return "Insert";
            break;
        case Delete:
            return "Delete";
            break;
        default:
            return "ERROR";
            break;
    }
}
class MStmt {
protected:
  QStringList _names;
  QVariantList _values;
public:
    void setValues(const QVariantList &values);
    void setNames(const QStringList &names);
    MStmt();
    MStmt(const MStmt &stmt);
    MStmt(MStmtKind kind, QString stmt ,QStringList names ,QVariantList values ={});
    QStringList &names();
    QVariantList &values();
    //QVector<NamedVal> vals;
    QString stmt;
    MStmtKind kind;
};
inline auto nullMStmt = MStmt();
struct MStmtBool{
    MStmt* mStmt;
    bool hasError;
};
QString $(MStmt self);

MStmt operator&&(MStmt s1,  MStmt &s2);

MStmt operator||(MStmt s1,  MStmt &s2);

MStmt operator!(MStmt s1);

MStmt operator==(MStmt s, const QVariant &val);

MStmt operator>(MStmt s, const QVariant &val);

MStmt operator>=(MStmt s, const QVariant &val);

MStmt operator<(MStmt s, const QVariant &val);

MStmt operator<=(MStmt s, const QVariant &val);

MStmt operator%(MStmt s, const QVariant &val);

MStmt filter(QStringList names, QVariantList vals = {});
MStmt filter(QString name);





