//
// Created by MERHAB on 03/08/2023.
//
#pragma once

#include "meta.h"
#include "MStmt.h"

class MetaList {
public:
protected:
    QVector<Meta *> _metas;
    QString *_tableName;
public:
    const QVector<Meta *> &getMetas() const;

    void setMetas(const QVector<Meta *> &metas);

    QString *getTableName();

    void setTableName(QString *tableName);

    void append(Meta *meta);

    void append(QVector<Meta *> metaList);

    int64_t size();

    MetaBool itemByName(QString name);

    MetaBool itemByIndex(int index);

    MStmt createTable(QString tableName, QString engine = "QSQLITE");

    bool isEmpty();

    MetaList(QString* tableName = nullptr);


};



