//
// Created by mac on 03/08/2023.
//

#include "MetaList.h"

void MetaList::append(Meta* meta) {
    meta->setColumn(this->_metas.size());
    this->_metas.append(meta);
}

void MetaList::append(QVector<Meta*> metaList) {
    for (auto meta: metaList) {
        meta->setColumn(this->_metas.size());
        this->append(meta);
    }
}

int64_t MetaList::size() {
    return this->_metas.size();
}

MetaBool MetaList::itemByName(QString name) {
    for (auto &meta: this->_metas) {
        if (meta->getName() == name) {
            return {meta, false};
        }
    }
    return {&nullMeta, true};
}

MetaBool MetaList::itemByIndex(int index) {
    if (index < 0 or index >= this->size())
        return {&nullMeta, true};
    return {this->_metas[index], false};
}

MetaList::MetaList(QString* tableName ) {
    this->_tableName = tableName;
    this->append({&id, &idMaster});
}

MStmt MetaList::createTable(QString tableName, QString engine) {
    QString kind = "";
    QStringList result;
    for (auto meta: this->_metas) {
        QString str = "";
        switch (meta->getType()) {
            case Int:
            case Int64:
                kind = "INTEGER";
                break;
            case String:
                kind = "TEXT";
                break;
            case Float:
                kind = "REAL";
                break;
            default:
                assert(false);
                break;

            case Null:
                assert(false);
                break;
        }
        str = "`" + meta->getName() + "` " + kind;
        if (meta->isPrimary())
            str = str + " PRIMARY KEY";
        if (meta->isAutoInc()) {
            if (engine == "QSQLITE") {
                str = str + " AUTOINCREMENT";
            } else if (engine == "QMYSQL") {
                str = str + " AUTO_INCREMENT";
            } else
                assert(false);
        }
        if (meta->isUnic())
            str = str + " UNIQUE";
        if (meta->isMondatory())
            str = str + " NOT NULL";
        if (meta->getDefaultVal().isValid()) {
            str = str + " DEFAULT ";
            if (meta->getType() == String)
                str = str + "\"" + meta->getDefaultVal().toString() + "\"";
            else
                str = str + meta->getDefaultVal().toString();
        }
        result.append(str);
    }


    MStmt stmt;
    stmt.kind = CreateTable;

    stmt.stmt =
            "CREATE TABLE IF NOT EXISTS `" + tableName + "`(" +
            result.join(",") + ")";
    return stmt;
}

bool MetaList::isEmpty() {
  return this->_metas.isEmpty();
}

const QVector<Meta *> &MetaList::getMetas() const {
    return _metas;
}

void MetaList::setMetas(const QVector<Meta *> &metas) {
    _metas = metas;
}

QString *MetaList::getTableName(){
    return _tableName;
}

void MetaList::setTableName( QString *tableName) {
    _tableName = tableName;
}
