#include "MLazyTable.h"

int64_t MLazyTable::getRecordCount() const {
    return _recordCount;
}

void MLazyTable::setRecordCount(int64_t recordCount) {
    _recordCount = recordCount;
}

int64_t MLazyTable::getLimit() const {
    return _limit;
}

void MLazyTable::setLimit(int64_t limit) {
    _limit = limit;
}

int64_t MLazyTable::getOffset() const {
    return _offset;
}

void MLazyTable::setOffset(int64_t offset) {
    _offset = offset;
}

const QVector<MTable *> &MLazyTable::getTables() const {
    return _tables;
}

void MLazyTable::setTables(const QVector<MTable *> &tables) {
    _tables = tables;
}
