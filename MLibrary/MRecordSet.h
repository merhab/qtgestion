//
// Created by MERHAB on 01/08/2023.
//
#pragma once
#include "MRecord.h"
#include "mdataset.h"
class MRecordSet {
protected:
  QVector<MRecord> records;
  MDataSet<MRecord> _dataset;
public:
    MRecordSet();
    QVector<MRecord> &getRecords();
     MDataSet<MRecord> &dataset() ;
};

