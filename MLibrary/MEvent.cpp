#include "MEvent.h"
#include <qvariant.h>

const QString &MEvent::name() const {
    return _name;
}

void MEvent::setName(const QString &name) {
    _name = name;
}

 int MEvent::type()  {
    return _type;
}

void MEvent::setType(int type) {
    _type = type;
}

void MEvent::setFunc(MEventFunc func) {
    _func = func;
}

void MEvent::execFunc(void* sender,void* receiver,QVariantList args) {
  _func(sender,receiver,args);
}

bool MEvent::execCanFunc(void* sender,void* receiver,QVariantList args) {
  auto ret= _func(sender,receiver,args);
  //todo: remove assertion to gain speed
    assert(!ret.isEmpty());
    assert(QString(ret[0].metaType().name())=="bool");
    return ret[0].toBool();
}

static int mEventNamingInc=0;
MEvent::MEvent(MEventFunc function, int type, QString name) {
    if(name ==""){
        mEventNamingInc++;
        name = "MEvent" + QString::number(mEventNamingInc);
    }
    this->_name = name;
    this->_type = type;
    this->_func = function;
}
