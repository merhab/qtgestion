//
// Created by MERHAB on 01/08/2023.
//

#pragma once

#include "MField.h"
#include "MStmt.h"
#include "MetaList.h"
#include "mdataset.h"
#include <qvariant.h>

struct MFieldBool {
  MField *fld;
  bool hasError;
};
class MRecord {
public:
  MRecord();

  MRecord(MetaList &metaList);
  MRecord(QVector<Meta*> metas);

  MField& fieldByName(QString name);

  MField& fieldByIndex(int index);

  MDataSet<MField> &getFieldset();

  void setFieldset(const MDataSet<MField> &fieldset);

   QVector<MField> &getFields() ;

  void setFields(const QVector<MField> &fields);

  int64_t size();

  MField *itemAt(int index);

  void append(MField fld);

  void append(QVector<MField> flds);

  MStmt insert(QString tableName, int64_t idMaster = -1);
  MStmt update(QString tableName);
  MStmt remove(QString tableName);
  int64_t getID();
  int64_t getIDMaster();
  MField &getIDField();
  MField &getIDMasterField();
  MField &getFirstField();
  int firstIndex();
  bool isDirty() const;

  void setDirty(bool dirty);

  bool isDeleted() const;

  void setDeleted(bool deleted);
  bool isNew();
  QString names();
  QStringList namesList();
  void setValues(QVector<QVariant> vals);
protected:
  QVector<MField> fields;
  MDataSet<MField> fieldset;
  bool dirty = false;
  bool deleted = false;
};

QString $d(MRecord);
QString $(MRecord rd);
