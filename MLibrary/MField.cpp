//
// Created by MERHAB on 31/07/2023.
//
#include "MField.h"
#include <cassert>

MField::MField(Meta* meta, QVariant val) {
  this->meta = meta;
  this->setValue(val);
}

MField::MField(Meta *meta) {
    this->meta = meta;
    this->value = meta->initVal();
}

MField::MField(const MField& fld) {
  this->meta = fld.meta;
  this->value = fld.value;
}

Meta* MField::getMeta() {
  return this->meta;
}

QVariant MField::getValue() {
  return this->value;
}

void MField::setMeta(Meta* meta) {
  this->meta = meta;
}

void MField::setValue(QVariant value) {
    qDebug()<<this->meta->getQtType() << " == " << QString(value.metaType().name()) <<"\n";
    //assert(this->meta->getQtType()== QString(value.metaType().name()));
    if (this->meta->getType() == Int or this->meta->getType() == Int64){
        assert(QString(value.metaType().name()) == "int"
        or QString(value.metaType().name()) == "qlonglong");
    } else if(this->meta->getType() == String) assert(QString(value.metaType().name()) == "QString");
    else if(this->meta->getType()==Float) assert(QString(value.metaType().name())=="float");
    else if(this->meta->getType()==Null) assert(QString(value.metaType().name())=="");
    else
        assert(false);
  this->value = value;
}


QString $(MField fld){
    return "{meta:"+ $(*fld.getMeta())+",val:"+fld.getValue().toString()+"}\n";
}

MField *newMField(Meta *meta, QVariant val) {
    return new MField(meta,val);
}
