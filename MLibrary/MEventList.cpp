//
// Created by MERHAB on 19/08/2023.
//

#include "MEventList.h"
#include <cassert>

static int mEventListNamingInc=0;
MEventList::MEventList(void* sender,void* receiver,QString name) {
  this->_receiver = receiver;
    this->_sender = sender;
  if(name==""){
      mEventListNamingInc++;
      name = "MEventList"+QString::number(mEventListNamingInc);
  }
  this->_name = name;
}

QString MEventList::name() {
  return _name;
}

MEventList& MEventList::registerEvent(MEvent event) {
  for(auto ev:this->eventList){
    if(ev.name()==event.name())
      assert(false);//cant have same name
  }
  this->eventList.append(event);
    return *this;
}

void MEventList::unregisterEvent(QString name) {
  for(int i=0;i<this->eventList.size();i++){
    if(this->eventList[i].name()==name){
      this->eventList.remove(i);
    }
  }
}

void MEventList::runEvent(int eventType,QVariantList args) {
  if(!_isConnect) return;
  for(auto& ev:this->eventList){
    if(ev.type()==eventType){
      ev.execFunc(_sender,_receiver,args);
    }
  }
}


bool MEventList::runCanEvent(int eventType,QVariantList args) {
  if(!_isConnect) return true;
  for(auto& ev:this->eventList){
    if(ev.type()==eventType){
      if(!ev.execCanFunc(_sender,_receiver,args)){
	return false;
      }
    }
  }
  return true;
}

void MEventList::connect() {
  this->_isConnect = true;
}

void MEventList::disconnect() {
  this->_isConnect = false;
}

void MEventList::setReceiver(void *receiver) {
    _receiver = receiver;
}

void MEventList::setSender(void *sender) {
    _sender = sender;
}

bool MEventList::isGuiConnected() {
    return _isConnect;
}

void MEventList::setName(const QString &name) {
    _name = name;
}




