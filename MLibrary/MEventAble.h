//
// Created by MERHAB on 27/08/2023.
//
#pragma once
#include "MEventList.h"

class MEventAble {
protected:
    QVector<MEventList> events;
public:
    void appendEventList(MEventList eventList);
    void removeEventList(QString name);
    bool runCanEvents(int type,QVariantList args={});
    void runEvents(int type,QVariantList args={});
};
 
