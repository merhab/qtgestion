//
// Created by MERHAB NOUREDDINE on 06/09/2023.
// Copyright (c) 2023 MERHAB NOUREDDINE. All rights reserved.
//

#pragma once
#include "MVector.h"
template<typename T>
class MArrayMemory{
    MVector<T> _memory={};
    MVector<size_t> _deleted={};
public:
    void deleteItem(T* item){
        for (size_t i = 0; i < _memory.count() ; ++i) {
            if(_memory[i]== item){
                _deleted.append(i);
                return ;
            }
        }
    }


    T* newItem(T &item){
        if(_deleted.isEmpty()){
            _memory.append(item);
            return &(_memory[_memory.count()-1]);
        } else{
            size_t idx = _deleted[_deleted.count()-1];
            _memory[idx] = item;
            _deleted.removeItem(_deleted.count()-1);
            return &(_memory[idx]);
        }
    }

    MArrayMemory(){
        _memory={};
        _deleted={};
    }
    MArrayMemory(const MArrayMemory& memory){
        this->_memory = memory._memory;
        this->_deleted = memory._deleted;
    }

    MArrayMemory& operator=(const MArrayMemory& memory){
        if (this == &memory) return *this;
        this->_memory = memory._memory;
        this->_deleted = memory._deleted;
    }
};