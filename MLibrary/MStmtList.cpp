//
// Created by MERHAB on 01/08/2023.
//

#include "MStmtList.h"
#include "MStmt.h"
#include "meta.h"
#include <cassert>
#include <qforeach.h>
#include <qvariant.h>

MStmtList::MStmtList() = default;

MStmtList::MStmtList(const MStmtList &list) {
    this->list = list.list;
    this->cmdType = list.cmdType;
}

MStmtList &MStmtList::prepare() {
    MStmt s;
    switch (this->cmdType) {
        case SelectCmd: {
            auto result = this->locate(Select);
            if (!result.hasError) {
                s = *result.mStmt;
            } else {
                assert(false);
            }
            break;
        }
        case UpdateCmd: {
            auto result = this->locate(Update);
            if (!result.hasError) {
                s = *result.mStmt;
            } else {
                assert(false);
            }
            break;
        }
        case InsertCmd: {
            auto result = this->locate(Insert);
            if (!result.hasError) {
                s = *result.mStmt;
                preparedStmt = s;
                return *this;
            } else {
                assert(false);
            }
            break;
        }
        case DeleteCmd: {
            auto result = this->locate(Delete);
            if (!result.hasError) {
                s = *result.mStmt;
            } else {
                assert(false);
            }
            break;
        }
        case CreateTableCmd: {
            auto result = this->locate(CreateTable);
            if (!result.hasError) {
                preparedStmt = *result.mStmt;
                return *this;
            } else {
                assert(false);
            }
            break;
        }
    }

    //*************************

    QVector<MStmt> filterList;
    QVector<MStmt> orderByList;
        for(auto stmt:this->getList()){
      if(stmt.kind == AndFilter or stmt.kind == OrFilter){
	filterList.append(stmt);
      }
      if(stmt.kind == OrderBy){
	orderByList.append(stmt);
      }
    }
	
    //*************************
    //*****filters*************
    //*************************
   
    if(!filterList.isEmpty()){
      QString str = "("+filterList[0].stmt+")";
      QVariantList vals;
      QStringList names;
      vals.append(filterList[0].values());
      names.append(filterList[0].names());
      for(int i=1;i<filterList.size();i++){
	if(filterList[i].kind == AndFilter){
	  str += " AND ("+filterList[i].stmt+")";
	}else{
	  str += " OR ("+filterList[i].stmt+")";
	}
          vals.append(filterList[i].values());
          names.append(filterList[i].names());
      }
      s.stmt += " WHERE "+str;
      s.values().append(vals);
      s.names().append(names);
    }
    /*
      END FILTER
    */
   
     //*************************
    //*****××ORDERBY************
    //*************************
    if(!orderByList.isEmpty()){
      QString str = orderByList[0].names()[0];
      if(orderByList[0].values()[0] == 0){
	//ASC|DESC
	str += " DESC";
      }else{
	str+= " ASC";
      }
      //we work in sort that each stmt has only one order by;
      // so it is vals[0];
      for(int i =1 ; i< orderByList.size();i++){
	str +=","+ orderByList[i].names()[0];
      if(orderByList[i].values()[0] == 0){
	str += " DESC";
      }else{
	str+= " ASC";
      }
      }
      s.stmt +=" ORDER BY "+str;
    }
    
    if (limit > -1) {
        s.stmt += " LIMIT " + QString::number(limit);
        if (offset > -1) {
            s.stmt += " OFFSET " + QString::number(offset);
        }
    }


    preparedStmt = s;
    return *this;
}

MStmtBool MStmtList::locate(MStmtKind kind) {
    MStmtBool b = {&nullMStmt, true};
    for (auto &item: this->list) {
        if (item.kind == kind) {
            b.mStmt = &item;
            b.hasError = false;
            break;
        }
    }
    return b;
}


MStmtList& MStmtList::append(MStmt stmt) {
    this->list.append(stmt);
    return *this;
}

MStmtBool MStmtList::itemAt(int index) {
    if (index < 0 or index >= this->list.size()) return {&nullMStmt, true};
    return {&this->list[index], false};
}

int MStmtList::size() {
    return this->list.size();
}

MStmt *MStmtList::getLast() {
    if (this->list.isEmpty()) return nullptr;
    return &(this->list[this->list.size() - 1]);
}

const QVector<MStmt> &MStmtList::getList() const {
    return list;
}

void MStmtList::setList(const QVector<MStmt> &list) {
    MStmtList::list = list;
}

CmdType MStmtList::getCmdType() const {
    return cmdType;
}

MStmtList& MStmtList::setCmdType(CmdType cmdType) {
    MStmtList::cmdType = cmdType;
    return *this;
}

MStmt &MStmtList::getPreparedStmt()  {
    return preparedStmt;
}

void MStmtList::setPreparedStmt(const MStmt &preparedStmt) {
    MStmtList::preparedStmt = preparedStmt;
}

int64_t MStmtList::getIdMaster() const {
    return idMaster;
}

MStmtList& MStmtList::setIdMaster(int64_t idMaster) {
    MStmtList::idMaster = idMaster;
    return *this;
}

int64_t MStmtList::getLimit() const {
    return limit;
}

void MStmtList::setLimit(int64_t limit) {
    MStmtList::limit = limit;
}

int64_t MStmtList::getOffset() const {
    return offset;
}

void MStmtList::setOffset(int64_t offset) {
    MStmtList::offset = offset;
}

bool MStmtList::isEmpty() {
  return this->getList().isEmpty();
}

QString $(MStmtList list) {
    QString s="{";
    for(auto stmt:list.getList()){
        s+=$(stmt)+",";
    }
    return s+"}";
}
