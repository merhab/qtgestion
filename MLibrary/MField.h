//
// Created by MERHAB on 31/07/2023.
//

#pragma once
#include "meta.h"
#include <qvariant.h>
#include <sys/_types/_int64_t.h>
class MField{
protected:
    Meta* meta;
    QVariant value;
public:
  MField(Meta* meta,QVariant val);
  MField(const MField& fld);
  MField(Meta* meta);
  Meta* getMeta();
  QVariant getValue();
  void setMeta(Meta* meta);
  void setValue(QVariant value);
};
QString $(MField fld);
MField *newMField(Meta* meta,QVariant val);