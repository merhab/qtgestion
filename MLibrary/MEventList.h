//
// Created by MERHAB on 19/08/2023.
//
#pragma once
#include "MEvent.h"

class MEventList {
protected:
  QString _name;
  void* _receiver=0;
  void* _sender=0;
    QVector<MEvent> eventList;
  bool _isConnect = true;
public:
    MEventList(void* sender=0,void* receiver=0,QString name="");
  QString name();
    void setName(const QString &name);
    MEventList& registerEvent(MEvent event);
  void unregisterEvent(QString name);
  void runEvent(int eventType,QVariantList args={});
  bool runCanEvent(int eventType,QVariantList args={});
  void connect();
  void disconnect();
  void setReceiver(void* receiver);
  void setSender(void* sender);
  bool isGuiConnected();


};




