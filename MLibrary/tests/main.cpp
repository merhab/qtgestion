//
// Created by MERHAB on 02/08/2023.
//
#include "meta_t.h"
#include "MField_t.h"
#include "Mrecord_t.h"
#include "MStmt_t.h"
#include "MdataSet_t.h"
#include "mtable_t.h"
int main() {
    testMdataset();
    testMeta();
    testMField();
    testMRecord();
    testMStmt();
    testMTable();
    return 0;
}
