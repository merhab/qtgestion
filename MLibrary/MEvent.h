#pragma once
#include <QString>
#include <QVector>
#include <QVariantList>
typedef QVariantList (*MEventFunc)(void*, void*, QVariantList) ;
class MEvent{
public:
    MEvent(MEventFunc function, int type, QString name="");

    const QString &name() const;

  void setName(const QString &name);

  int type();

  void setType(int type);

  void setFunc(MEventFunc func);

  void execFunc(void* sender,void* receiver,QVariantList args ={});

  bool execCanFunc(void* sender,void* receiver,QVariantList args={});

protected:
  QString _name;
  int _type;
  MEventFunc _func;
};
