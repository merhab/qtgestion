//
// Created by MERHAB on 02/08/2023.
//

#include "MTable.h"
#include "MField.h"
#include "MRecord.h"
#include "MStmt.h"
#include "MStmtList.h"
#include "MetaList.h"
#include "meta.h"
#include <cassert>
#include <cstdint>
#include <qsqldatabase.h>
#include <qsqlquery.h>
#include <qvariant.h>
bool MTable::insert(MRecord *record) {
  //-----------before events--------------------
  if(!runCanEvents(MTableCanMove,{this->index(),this->dataSet().size()})) return false;
  if(!runCanEvents(MTableCanInsert)) return false;
  runEvents(MTableBeforeInsert);
  runEvents(MTableBeforeMove,{this->index(),this->dataSet().size()});
  //--------------------------------------------
  MStmtList stmtList;
  stmtList.setCmdType(InsertCmd);
  stmtList.setIdMaster(this->idMaster);
  auto stmt = record->insert(this->_tableName, this->idMaster);
  qDebug() << $(stmt);
  stmtList.append(stmt);
  this->stmtsList.append(stmtList);
  QSqlQuery query(*this->db);
  query.prepare(stmt.stmt);
  for (auto var : stmt.values()) {
    query.addBindValue(var);
  }
  if (query.exec()) {
    record->getIDField().setValue(query.lastInsertId());
    record->setDirty(false);
    //-------------after events------------------
      runEvents(MTableAfterInsert);
      runEvents(MTableAfterMove,{this->index()});
    //-------------------------------------------  
    return true;
  } else {
    qDebug() << query.lastError().text();
    return false;
  }
}

bool MTable::update(MRecord *record) {
  //----------------before events-----------------
  if(!runCanEvents(MTableCanUpdate)) return false;
  runEvents(MTableBeforeUpdate);
  //----------------------------------------------
  auto stmt = record->update(_tableName);
  this->stmtsList.append(MStmtList()
                             .setIdMaster(this->idMaster)
                             .setCmdType(UpdateCmd)
                             .append(stmt));

  QSqlQuery query(*this->db);
  query.prepare(stmt.stmt);
  for (auto var : stmt.values()) {
    query.addBindValue(var);
  }
  if (query.exec()) {
      record->setDirty(false);
      //-----------------after Events------------
      runEvents(MTableAfterUpdate);
      //-----------------------------------------
    return true;
  } else {
    qDebug() << query.lastError().text();
    return false;
  }
}

bool MTable::remove(MRecord *record) {
  //-----------before events--------------------
  int64_t ind;
  if(this->dataSet().size()==1) ind =-1;
  else if(this->index()==0) ind = 0;
  else ind = this->index()-1;
  if(!runCanEvents(MTableCanMove,{this->index(),ind})) return false;
  if(!runCanEvents(MTableCanRemove)) return false;
  runEvents(MTableBeforeRemove);
  runEvents(MTableBeforeMove,{this->index(),ind});
  //--------------------------------------------
  auto stmt = record->remove(_tableName);
  this->stmtsList.append(MStmtList().setCmdType(DeleteCmd).append(stmt));
  QSqlQuery query(*this->db);
  query.prepare(stmt.stmt);
  for (auto var : stmt.values()) {
    query.addBindValue(var);
  }
  if (query.exec()) {
      record->setDeleted(true);
      //-------------after events------------------
      runEvents(MTableAfterRemove);
      runEvents(MTableAfterMove,{this->index()});
      //-------------------------------------------  
    return true;
  } else {
    qDebug() << query.lastError().text();
    return false;
  }
}

MTable &MTable::select(QVector<Meta *> metas) {
  if (metas.isEmpty()) {
    QVector<Meta *> metaList;
    for (auto meta : this->getMetaList().getMetas()) {
      if (meta->isGenerated()) {
        metaList.append(meta);
      }
    }
//    for(auto meta:metaList) {
//        qDebug() << $(*meta);
//    }
    return MTable::select(metaList);
  } else {
    QStringList names;
    QString s = metas[0]->getName();
    names.append(metas[0]->getName());
    for (int i = 1; i < metas.size(); i++) {
      s += "," + metas[i]->getName();
      names.append(metas[i]->getName());
    }
    MStmtList stmts =
        MStmtList()
            .setCmdType(SelectCmd)
            .setIdMaster(this->idMaster)
            .append(
                {Select, "SELECT " + s + " FROM " + this->_tableName, names});
    this->names = names;
    if (this->idMaster > -1) {
      stmts.append({AndFilter,
                    IDMaster + "=?",
                    {{IDMaster}},
                    {{QVariant(this->idMaster)}}});
    }
    this->stmtsList.append(stmts);
  }

  return *this;
}

MTable &MTable::filter(MStmt filter, MStmtKind kind) {
  auto res = this->getLastSTmts();
  if (res.hasError) {
    assert(false);
  } else {
    if (res.stmts->getCmdType() != SelectCmd) {
      // for now we only filter select stmt
      assert(false);
    } else {
      filter.kind = kind;
      res.stmts->append(filter);
    }
  }
  return *this;
}

MTable &MTable::orFilter(MStmt filter) {
  return MTable::filter(filter, OrFilter);
}

MTable &MTable::andFilter(MStmt filter) {
  return MTable::filter(filter, AndFilter);
}

MStmtListBool MTable::getLastSTmts() {
  if (this->stmtsList.isEmpty()) {
    return {&nullMStmtList, true};
  }
  return {&this->stmtsList[this->stmtsList.size() - 1], false};
}

MTable::MTable(QSqlDatabase *db, QString tableName, QVector<Meta*> metaList) {
  this->_tableName = tableName;
  for(auto meta:metaList){
    meta->setColumn(this->metaList.size());
    this->metaList.append(meta);
  }
  this->db = db;
}

const QVector<MStmtList> &MTable::getStmts() const { return stmtsList; }

const QString &MTable::tableName() const { return _tableName; }

int64_t MTable::getIdMaster() const { return idMaster; }

MetaList &MTable::getMetaList() { return metaList; }

void MTable::setIdMaster(int64_t idMaster) { MTable::idMaster = idMaster; }

SqlResult MTable::exec() {
  assert(!stmtsList.isEmpty());
  MStmtList &stmts = stmtsList[stmtsList.size() - 1];
  SqlResult b;
  QSqlQuery query(*this->db);
  query.prepare(stmts.prepare().getPreparedStmt().stmt);
  qDebug() << $(stmts.getPreparedStmt());
  for (int i = 0; i < stmts.getPreparedStmt().values().size(); i++) {
    qDebug() << stmts.getPreparedStmt().values()[i].toString() + "\n";
    query.addBindValue(stmts.getPreparedStmt().values()[i]);
  }
  if (!query.exec()) {
    b.type = SqlError;
    qDebug() << query.lastError().text() + "\n";
    return b;
  }
  if (this->getLastSTmts().stmts->getCmdType() == SelectCmd) {
      QVector<Meta*> metas;
      for (int i = 0; i < this->names.size(); i++){
          metas.append(this->metaList.itemByName(this->names[i]).meta);
          qDebug() << $(*metas[i]) << "\n";
      }
      this->dataSet().clear();
      while (query.next()) {
        MRecord rd(metas);
        for (int i = 0; i < this->names.size(); i++) {
            qDebug() << QString(query.value(i).metaType().name())+":"<< query.value(i).toString() << "\n";
          rd.getFields()[i].setValue(query.value(i));
        }
        this->dataSet().append(rd);
      }
      this->dataSet().first();
  }
  return b;
}

void MTable::cancel(MRecord *record) {
  assert(!record->getFields().isEmpty());
  if (record->isNew()) {
    record->setDirty(false);
    record->setDeleted(true);
    return;
  }
  if (record->isDirty()) {
    QSqlQuery query(*db);
    QString sql = "SELECT " + record->names() + " FROM " + _tableName +
                  "WHERE " + ID + "=" + QString::number(record->getID());
    if (query.exec(sql)) {
      if (query.next()) {
        for (int i = 1; i < record->getFields().size(); i++) {
          record->fieldByIndex(i).setValue(query.value(i));
        }
      }
    }
  }
}

SqlResult MTable::exec(QString sql, QVector<Meta *> metaList,
                       QVector<QVariant> bindingVals) {
  SqlResult b;
  QSqlQuery query(*db);
  bool sucess;
  if (bindingVals.isEmpty()) {
    sucess = query.exec(sql);
  } else {
    query.prepare(sql);
    for (auto val : bindingVals) {
      query.addBindValue(val);
    }
    sucess = query.exec();
  }
  if (sucess) {
    if (query.isValid()) {
      b.type = SqlRecordSet;
      while (query.next()) {
        MRecord rd;
        for (int i = 0; i < metaList.size(); i++) {
          rd.append({metaList[i], query.value(i)});
        }
        b.recordset.append(rd);
      }
    } else {
      b.type = SqlCommand;
    }
  } else {
    b.type = SqlError;
    b.error = query.lastError().text();
  }
  return b;
}

bool MTable::insert() {
  return insert(&this->_recordset.dataset().current());
}

bool MTable::update() {
    return update(&this->_recordset.dataset().current());
}

bool MTable::remove() {
    return remove(&this->_recordset.dataset().current());
}

MRecordSet &MTable::recordset() {
    return _recordset;
}

MDataSet<MRecord>& MTable::dataSet() {
  return this->recordset().dataset();
}

MRecord& MTable::current() {
  return this->recordset().dataset().current();
}

bool MTable::append(QVariantList vars) {
  this->dataSet().append(MRecord(this->metaList));
  this->current().setValues(vars);
  return this->insert();
}

void MTable::append() {
  this->dataSet().append(MRecord(this->metaList));
}

int64_t MTable::index() {
  return dataSet().currentIndex();
}

bool MTable::goTo(int64_t index) {
  if(!runCanEvents(MTableCanMove,{this->index(),index}))
    return false;
  runEvents(MTableBeforeMove,{this->index(),index});
  return dataSet().goTo(index);
  runEvents(MTableAfterMove,{this->index()});
  
}

bool MTable::last() {
  return goTo(this->dataSet().size()-1);
}

bool MTable::prior() {
  return goTo(this->index()-1);
}

bool MTable::next() {
  return goTo(this->index()+1);
}

bool MTable::first() {
  return goTo(0);
}




