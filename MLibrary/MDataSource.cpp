//
// Created by mac on 26/08/2023.
//

#include "MDataSource.h"

const QVector<MDbAbleGui *> &MDataSource::getDbControls() const {
    return _dbControls;
}

MTable *MDataSource::getTable() const {
    return _table;
}

MDataSource& MDataSource::setTable(MTable *table) {
    _table = table;
    MEventList list(table,this);
    list.registerEvent(MEvent(onMTableAfterMove,MTableAfterMove));
    table->appendEventList(list);
    return *this;
}

MDataSource& MDataSource::appendGui(MDbAbleGui *gui) {
    _dbControls.append(gui);
    MEventList list(gui,this);
    list.registerEvent(MEvent(onMdbAbleGuiValidate,MDbAbleGui::MdbAbleGuiValidate));
    gui->appendEventList(list);
    return *this;
}

QVariantList onMTableAfterMove(void* sender,void* receiver,QVariantList args){
    assert(receiver);
    MDataSource *dataSource = (MDataSource *) receiver;
    MTable* table = dataSource->getTable();
    for(auto gui:dataSource->getDbControls()){
        MField* fld = &table->current().fieldByName(gui->getFieldName());
        gui->setField(fld);
        gui->setValue(fld->getValue());
    }
    return {};
}

QVariantList onMdbAbleGuiValidate(void *sender, void *receiver, QVariantList args) {
    assert(sender);
    MDbAbleGui* gui = (MDbAbleGui*) sender;
    MDataSource* dataSource = (MDataSource*) receiver;
    if(gui->field()->getValue() != gui->value()) {
        gui->field()->setValue(gui->value());
        dataSource->getTable()->current().setDirty(true);
    }
    return {};
}
