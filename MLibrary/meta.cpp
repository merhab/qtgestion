//
// Created by mac on 31/07/2023.
//

#include "meta.h"

Meta::Meta(const QString &name, MSqlType type, bool primary, bool autoInc,
           bool unic, bool readOnly, Priority priority, bool generated,
           bool visible, const QVariant &defaultVal, QVariant col)
    : name(name), type(type), primary(primary), autoInc(autoInc), unic(unic),
      readOnly(readOnly), priority(priority), generated(generated),
      visible(visible), defaultVal(defaultVal), col(col) , caption(name) {}

Meta::Meta(const Meta &meta)
    : name(meta.name), type(meta.type), primary(meta.primary),
      autoInc(meta.autoInc), unic(meta.unic), readOnly(meta.readOnly),
      priority(meta.priority), generated(meta.generated), visible(meta.visible),
      defaultVal(meta.defaultVal), col(meta.col), caption(meta.caption) {}

Meta metaInt(const QString &name) {
  return Meta(name, Int, false, false, false, false, Normal, true, true,
              QVariant(), QVariant());
}

Meta metaStr(const QString &name) {
  return Meta(name, String, false, false, false, false, Normal, true, true,
              QVariant(), QVariant());
}

Meta metaFloat(const QString &name) {
  return Meta(name, Float, false, false, false, false, Normal, true, true,
              QVariant(), QVariant());
}

Meta metaInt64(const QString &name) {
  return Meta(name, Int64, false, false, false,
              false, Normal, true, true,
              QVariant(), QVariant());
}

QString $(Meta meta) {
    QString s;
    s = "{name:"+meta.getName()+
            ",type:"+sqlTypeToStr(meta.getType())+
            ",primary:"+QVariant(meta.isPrimary()).toString()+
            ",autoInc:"+QVariant(meta.isAutoInc()).toString()+
            ",unic:"+QVariant(meta.isUnic()).toString()+
            ",readOnly:"+QVariant(meta.isReadOnly()).toString()+
            ",priority:"+ priorityToStr(meta.getPriority())+
            ",generated:"+QVariant(meta.isGenerated()).toString()+
            ",visible:"+QVariant(meta.isVisible()).toString()+
            ",default val:"+meta.getDefaultVal().toString()+
            ",column:"+meta.getCol().toString()+"}\n";
    return s;
}

QString sqlTypeToStr(MSqlType type) {
    switch (type) {

        case Int:
            return "Int";
            break;
        case Int64:
            return "Int64";
            break;
        case String:
            return "String";
            break;
        case Float:
            return "Float";
            break;
        default:
            assert(false);
    }
}

QString priorityToStr(Priority p) {
    switch (p) {

        case Obligatory:
            return "Obligatory";
            break;
        case Complimentary:
            return "Complimentary";
            break;
        case Normal:
            return "Normal";
            break;
        default:
            assert(false);
    }
}

Meta *newMetaStr(const QString &name) {
    return new Meta(name, String, false, false, false, false, Normal, true, true,
                       QVariant(), QVariant());;
}

Meta *newMetaInt(const QString &name) {
    return new Meta(name, Int, false, false, false, false, Normal, true, true,
                    QVariant(), QVariant());
}

Meta *newMetaInt64(const QString &name) {
    return new Meta(name, Int64, false, false, false, false, Normal, true, true,
                    QVariant(), QVariant());
}

Meta *newMetaFloat(const QString &name) {
    return new Meta(name, Float, false, false, false, false, Normal, true, true,
                    QVariant(), QVariant());
}

Meta &Meta::setPrimary(bool isPrimary) {
  primary = isPrimary;
  return *this;
}

Meta &Meta::setAutoInc(bool isAutoInc) {
  this->autoInc = isAutoInc;
  return *this;
}

Meta& Meta::setUnic(bool isUnic) {
  this->unic = isUnic;
  return *this;
}

Meta& Meta::setReadOnly(bool isReadOnly) {
  this->readOnly = isReadOnly;
  return *this;
}

Meta& Meta::setMendatory() {
  this->priority = Obligatory;
  return *this;
}

Meta& Meta::setComplimentary() {
  this->priority = Complimentary;
  return *this;
}

Meta& Meta::setNormalPriority() {
  this->priority = Normal;
  return *this;
}

Meta& Meta::setNotGenerated() {
  this->generated = false;

  return *this;
}

Meta& Meta::setGenerated() {
  this->generated =true;
  return *this;
}

Meta& Meta::setInvisible() {
  this->visible =false;

  return *this;
}

Meta& Meta::setVisible() {
  this->visible = true;
  return *this;
}

Meta& Meta::setColumn(int column) {
  this->col = column;
  return *this;
}

Meta& Meta::setDefaultValue(QVariant value) {
  this->defaultVal = value;
  return *this;
}

QString Meta::getName() {
  return this->name;
}

QString Meta::getMasterFieldName() const {
    return masterFieldName;
}

void Meta::setMasterFieldName(const QString masterFieldName) {
    this->masterFieldName = masterFieldName;
}

QString Meta::getMasterTableName() const {
    return masterTableName;
}

Meta &Meta::setMasterTableName(const QString masterTableName) {
    this->masterTableName = masterTableName;
    return *this;
}

MSqlType Meta::getType() const {
    return type;
}

void Meta::setType(MSqlType type) {
    Meta::type = type;
}

bool Meta::isPrimary() const {
    return primary;
}

bool Meta::isAutoInc() const {
    return autoInc;
}

bool Meta::isUnic() const {
    return unic;
}

bool Meta::isReadOnly() const {
    return readOnly;
}

Priority Meta::getPriority() const {
    return priority;
}

bool Meta::isMondatory() {
    return this->getPriority() == Obligatory;
}


bool Meta::isGenerated() const {
    return generated;
}

bool Meta::isVisible() const {
    return visible;
}

const QVariant &Meta::getDefaultVal() const {
    return defaultVal;
}

const QVariant &Meta::getCol() const {
    return col;
}

const QVariant &Meta::getVisibleValue() const {
    return visibleValue;
}

QString Meta::getQtType() {
  QString b;
    switch (this->type) {
        case Int:
            b="int";
            break;
        case Int64:
            b="qlonglong";
            break;
        case String:
            b="QString";
            break;
        case Float:
            b="float";
            break;
        case Null:
            b="";
            break;
        default:
            assert(false);
    }
    return b;
}

QVariant Meta::initVal() {
    switch (this->type) {

        case Int:
            return 0;
            break;
        case Int64:
            return -1;//I use this only for Id , if used for anything else will cause error
            break;
        case String:
            return "";
            break;
        case Float:
            return float(0);
            break;
        case Null:
            return {};
            break;
        default:
            assert(false);
    }
}

const QString &Meta::getCaption() const {
    return caption;
}

Meta& Meta::setCaption(const QString &label) {
    Meta::caption = label;
    return *this;
}

