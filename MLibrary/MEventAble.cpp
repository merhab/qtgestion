//
// Created by mac on 27/08/2023.
//

#include "MEventAble.h"

void MEventAble::appendEventList(MEventList eventList) {
    for(auto evList:this->events){
        if(evList.name()==eventList.name()){
            assert(false);//we don`t want duplicated
        }
    }
    eventList.setReceiver(this);
    this->events.append(eventList);
}

void MEventAble::removeEventList(QString name) {
    for(int i=0 ;i<this->events.size();i++){
        if(this->events[i].name()==name){
            this->events.remove(i);
            return;
        }
    }
}

bool MEventAble::runCanEvents(int type, QVariantList args) {
    for(auto eventList:this->events){
        if(!eventList.runCanEvent(type,args))
            return false;
    }
    return true;
}

void MEventAble::runEvents(int type, QVariantList args) {
    for(auto eventList:this->events){
        eventList.runEvent(type,args);
    }
}
