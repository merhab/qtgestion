#include "mdb.h"
#include <QSqlError>
#include <iostream>
#include <qvariant.h>

int main(int argc, char *argv[]) {
  MStmt s = names({"ana", "anta"});
  s.names.append(QString("nour"));
  s.names.append("mer");
  s.vals.append(QVariant(10));
  s.vals.append(QVariant("settout"));
  MStmt s2;
  s2.names.append(QString("ali"));
  s2.names.append("toufik");
  s2.vals.append(QVariant("ninja"));
  s2.vals.append(QVariant("minouna"));
  foreach (QString s, s.names) {
    std::cout << s.toStdString() << "\n";
  }
  foreach (QVariant v, s.vals) {
    std::cout << v.typeName() << "\n";
  }
  MetaDb m("ID", Int64);
  QList<MetaDb> ml;
  ml.append(
      {*MetaDb("name", String).setDefaultVal("noName"), MetaDb("age", Int)});
  QString ss = ml[0].getName();
  ss = ml[1].getName();

  MetaDbList list;
  for (int i = 0; i < ml.size(); ++i) {
    list.append(&ml[i]);
  }
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("/Users/mac/dev/test.db");
  if (!db.open()) {
    QString str = db.lastError().text();
    printf("%s\n", str.toStdString().data());
    assert(false);
  } else
    printf("%s", "connected\n");
  STable tbl = STable("user", db, list);
  if (tbl.createTable()->exec())
    printf("%s", "table creates\n");
  else {
    QString str = db.lastError().text();
    printf("%s\n", str.toStdString().data());
    assert(false);
  }
  for (int i = 0; i < 1000; ++i) {
    (tbl.insert({"noureddine" + QString::number(i), i})->exec());
    // printf("record %d inserted,\n",i);
  }
  if (tbl.remove()->filter(MStmt(ID) > 10 and MStmt(ID) < 6000)->exec())
    printf("record %d deleted,\n", 10);
  else {
    QString str = db.lastError().text();
    printf("%s\n", str.toStdString().data());
    assert(false);
  }

  if (tbl.update({"ali touhami", 45})
          ->filter(MStmt(ID) > 1 and MStmt(ID) < 10)
          ->exec(true))
    printf("record %d Updated,\n", 10);
  else {
    QString str = tbl.query.lastError().text();
    printf("%s\n", str.toStdString().data());
    assert(false);
  }

  if (tbl.select()->filter(MStmt("name") % "%ali%")->exec(true)) {
    while (tbl.query.next()) {
      QVariantList l;
      for (int i = 0; i < tbl.selectedNames().size(); ++i) {
        l.append(tbl.query.value(i));
      }

      MRecord rec(l, tbl.generatedMeta());
      out << red(tbl.query.value(0).toString()) << " "
          << tbl.query.value(2).toString() << "\n";
    }
    out << green("we get " + QString::number(tbl.query.numRowsAffected()) +
                 " rows\n");
  } else
    assert(false);

  return 0;
}
