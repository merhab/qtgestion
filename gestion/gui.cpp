#include "gui.h"
#include <cassert>
#include <qlabel.h>
#include <qvariant.h>
MInput::MInput()
{
}



QVariant MDbWidget::getVal() {
  return _val;
}

void MDbWidget::setVal(QVariant var) {
  this->_val = var;
}

void MLabel::setVal(QVariant var) {
  QLabel::setText(var.toString());
  MDbWidget::setVal(var);
}

void MLabel::setText(const QString &text) {
    MLabel::setVal(text);
}


void MDbWidget::val(const QVariant value){
  _val = value;
}

  int MDbWidget::column() const {
    return _column;
  }

  void MDbWidget::setColumn(const int _column) {
    this->_column = _column;
  }

void MLineEdit::setVal(QVariant var) {
    MDbWidget::setVal(var);
    QLineEdit::setText(var.toString());
}

void MLineEdit::setText(const QString &text){
    MLineEdit::setVal(text);
}

void MLineEdit::changeEvent(QEvent * event) {
    _val = this->text();
    QLineEdit::changeEvent( event);
}

MLayout::MLayout() {
  layoutBody.addWidget(&frmTop);
  layoutBody.addWidget(&_frmBody);
  layoutBody.addWidget(&frmBottom);
  frmBottom.setMaximumHeight(50);
  frmBottom.setMaximumHeight(50);
  frmTop.setLayout(&layoutTop);
  _frmBody.setLayout(&layoutBody);
  frmBottom.setLayout(&layoutBottom);
  
}

QFrame &MLayout::frmBody() {
    return this->_frmBody;
}
