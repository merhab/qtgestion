#pragma once
#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include <QWidget>
#include <QListWidget>
#include <qboxlayout.h>
#include <qframe.h>
#include <qgridlayout.h>
#include <qlabel.h>
#include <qvariant.h>
#include <QVector>
#include <QStringList>
#include <qwidget.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QMainWindow>
#include "mdataset.h"

class MDbWidget{
 protected:
  QVariant _val;
  int _column;
 public:
  int column() const;

  void setColumn(const int _column);

  QVariant getVal();

  void val(const QVariant _val);


  void setVal(QVariant var);


};

class MLabel:public QLabel,MDbWidget{
 protected:

 public:
  void setVal(QVariant var);
 void setText(const QString &text);

};
class MLineEdit:public QLineEdit,MDbWidget{

public:
    void setVal(QVariant var);
    void setText(const QString &text);
    void changeEvent(QEvent *event) override;

};
enum WidgetType{
  Label,
  Edit,
  ComboBox,
  CheckBox,
  ListWidget
};
class MInput{
 protected:
  QWidget* widget;
 public:
  MInput();
};

class MCommand{
    QVector<QVariant> (*command)(QFrame* frmBody,QFrame* frmTop,QFrame* frmBottom,QVector<QVariant>);
  QString name;
  QString type;
  QString module;
  QString shortcut;
  QStringList hooks;
};

class MLayout{
protected:
  QVBoxLayout layoutTop;
  QHBoxLayout layoutMain;
  QHBoxLayout layoutLeft;
  QHBoxLayout layoutRight;
  QVBoxLayout layoutBottom;
  QGridLayout layoutBody;
  QFrame _frmBody;
  QFrame frmTop;
  QFrame frmBottom;
public:
  MLayout();
  QFrame& frmBody();
};

typedef MDataSet<QVector<MCommand*>> MCmdList;
class MBuffer{
  QWidget container;
  MLayout layout;
  MCmdList cmdList;
};


typedef MDataSet<QVector<MBuffer*>> MBufferList;
class MMainWindow:public QMainWindow{
    MBufferList bufferList;
    MCmdList cmdList;
};
