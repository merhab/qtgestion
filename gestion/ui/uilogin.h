#ifndef UILOGIN_H
#define UILOGIN_H

#include <QDialog>
#include <QWidget>

namespace Ui {
class uiLogin;
}

class uiLogin : public QDialog
{
    Q_OBJECT

public:
    explicit uiLogin(QWidget *parent = nullptr);
    ~uiLogin();

private:
    Ui::uiLogin *ui;
};

#endif // UILOGIN_H
