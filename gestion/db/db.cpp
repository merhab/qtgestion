#include "db.h"

MetaDbList PersonMeta::toMetaDbList() {
    MetaDbList list;
    list.append(&this->name);
    list.append(&this->address);
    return list;
}
