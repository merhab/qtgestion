#pragma once

class PersonFieldNames{
     const QString name = "name";
};

class TelTypesMeta{
    MetaDb type = MetaDb("type",String).setMondatory();
};

class TelMeta{
    MetaDb type = MetaDb("type",String).setIsComplimentary();
    MetaDb tel = MetaDb("tel",String).setMondatory();

};
class PersonMeta{
    MetaDb name = MetaDb("name",String).setMondatory();
    MetaDb address =MetaDb("address",String).setIsComplimentary();
    MetaDb tel =MetaDb("tel",String).setIsComplimentary();

public:
    MetaDbList toMetaDbList();
};
